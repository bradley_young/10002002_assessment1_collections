﻿using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QUEUE
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue<char> q = new Queue<char>(); // creates queue
            q.Enqueue('B');
            q.Enqueue('Q');
            q.Enqueue('L');
            q.Enqueue('P');

            Console.WriteLine("Current queue: "); // displays current queue to the user
            foreach (char c in q) Console.Write(c + " ");

            Console.WriteLine();
            q.Enqueue('H');
            q.Enqueue('U');
            Console.WriteLine("Current queue: "); // enqueue adds values to the end of the queue
            foreach (char c in q) Console.Write(c + " ");

            Console.WriteLine();
            Console.WriteLine("Removing some values "); // removes values from the front of the queue 
            char ch = (char)q.Dequeue();
            Console.WriteLine("The removed value: {0}", ch);
            ch = (char)q.Dequeue();
            Console.WriteLine("The removed value: {0}", ch);

            Console.WriteLine();
            Console.WriteLine("Displaying amount of items in queue "); // counts values in queue
            char hc = (char)q.Count;
            Console.WriteLine("The amount counted: {0}", q.Count);

            Console.WriteLine("Does the queue contain L?"); // checks queue for certain value
            Console.WriteLine(q.Contains('L'));


            Console.WriteLine();
            Console.WriteLine("The values in this queue are: "); // prints queue 
            foreach (char c in q) Console.Write(c + " ");


            char[] arr = q.ToArray(); // copies queue to array
            foreach (char number in arr)
            {
                Console.WriteLine(number);
            }
            Console.ReadKey();
        }
    }
}
